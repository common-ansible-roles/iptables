# Ansible Role to install Docker

Tested on:
- CentOS 7
- Debian Jessie
- Debian Stretch
- Ubuntu Xenial
- Ubuntu Bionic

Example usage is included in `test/test.yml`.


License
-------

MIT
